defmodule BulbWeb.Statechart do
  use GenServer

  def start_link(ui), do: GenServer.start_link(__MODULE__, ui)

  def init(ui) do
    {:ok, %{ui_pid: ui}}
  end

  def handle_cast(:switch, state) do
    IO.puts "Hi, from Statechart"
    {:noreply, state}
  end
end
