defmodule BulbWeb.PageLive do
  use BulbWeb, :live_view

  def mount(_params, _session, socket) do
    {:ok, assign(socket, lamp_is_on: false)}
  end

  def handle_event("switch", _session, socket) do
    new_status = not socket.assigns.lamp_is_on
    {:noreply, assign(socket, lamp_is_on: new_status)}
  end
end
